import React from "react";
import "./App.css";
import RouteX from "./routes/Route.js";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuBar: false
    };
  }
  render() {
    const url = window.location.pathname;
    if (url === '/' || url === '/menu' || url === '/qr' || url === '/input-number') {
      return (
        <>

        <RouteX />
        </>
        
      );
    } else {
      return (
        <>
        <div className="container-fluid p-0 m-0">
          <div className="row p-0 m-0">
            <div className="col p-0 m-0">
              <div className="card" style={{ border: "none", background: "#D61F26", borderRadius: "0", minHeight: "100vh" , height:"100%" }}>
                <RouteX />
              </div>
            </div>
          </div>
        </div>
        </>
       
      );
    }
  };
}
export default App;
