import React, { Component } from "react";
import "./welcome.css";
import logo from "../../img/logo_ETRAN.png";
import MYRA_RED from "../../img/x1.5/MYRA_RED.png";
import { loginGuest_API } from "../../services/login/login_service.js";
import Error from "../error";
class Welcome extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    loginGuest_API().then(
      result => {
        localStorage.setItem('Access_', result['data']['id']);
        if(localStorage.getItem('Access_')){
          setTimeout(() => {
            window.location.href = "/menu";
          }, 1500);
        }else{
          setTimeout(() => {
            window.location.href = "/error";
          }, 1500);
        }
          
      });
  }
  render() {
    
    return (
      <div className="container-fluid content">
        <div className="row">
          <div className="col text-center mt-5">
            <img className="logo" src={logo} alt="Logo" />
          </div>
        </div>
        <div className="row middle">
          <div className="col-12 text-center">
            <h1>
              <b>ยินดีต้อนรับ</b>
              <img className="MYRA_logo" src={MYRA_RED} alt="Logo" />
            </h1>
            <h4 className="center-des1">
              พร้อมลุยแล้วหรือยัง
            </h4>
          </div>
        </div>
        <div className="row bottom">
          <div className="col text-center">
            <p>ขอให้เป็นวันที่ดีสำหรับคุณ :)</p>
          </div>
        </div>
      </div>
    );
  }
}
export default Welcome;
