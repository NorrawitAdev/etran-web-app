import React, { Component } from "react";
import { BottomSheet } from 'react-spring-bottom-sheet'
import 'react-spring-bottom-sheet/dist/style.css'
import loading_pic from "../../img/loading.gif"
import close from "../../img/x1/close.png"
import error_icon from "../../img/x2/error_icon.png"
import "./bottom_sheet.css";
import { stationQueueDetail } from "../../services/station/station.js";
import { maintenanceOnservice, maintenanceFinish, maintenanceConfirm, maintenanceReject } from "../../services/maintenanceService/maintenanceAPI.js"
import { spawBatteryOnservice, spawBatteryFinish, swapBatteryConfirm, swapBatteryReject } from "../../services/swapBatteryService/swapBatteryAPI.js"
import SwiperCore, {
    Pagination,
} from 'swiper';
import moment from 'moment';
import "moment/locale/th";
moment.locale('th');
SwiperCore.use([Pagination]);
class BottomSheetx extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            step: 1,
            stationQueueId: null,
            from: localStorage.getItem('comeFrom'),
            setWhat: null,
            stationData: {},
            queuetime1: '',
            queuetime2: '',
            unixTime: JSON.parse(localStorage.getItem('unixTime')),
            hourPlusUnix: sessionStorage.getItem('hourPlusUnix'),
            timeLocalNowUnix: sessionStorage.getItem('timeLocalNowUnix'),
            btnStyle: {},
            index: '',
        };
        this.setOpen = this.setOpen.bind(this);
        this.setOnservice = this.setOnservice.bind(this);
        this.hideBtn = this.hideBtn.bind(this);
    }
    async componentDidUpdate(prevProps) {
        if (prevProps.stationQueueId !== this.props.stationQueueId) {
            this.setState({
                setWhat: this.props.setWhat,
                stationQueueId: this.props.stationQueueId,
                index: localStorage.getItem('timeTempSelect')
            });
        }
    }
    async setOpen(val) {
        if (val) {
            setTimeout(() => {
                this.setState({ step: 1, open: val });
            }, 100);
            if (this.props.stationQueueId) {
                let result = this.props.stationQueueId
                await stationQueueDetail(result).then(
                    response => {
                        response = response.data;
                        if (response) {
                            let stationData = JSON.parse(sessionStorage.getItem('station'));
                            if (response.station.stationId === stationData.stationId) {
                                if (this.state.from === 'battery') {
                                    this.setState({
                                        queuetime1: moment(response.queueTime).format('H:mm'),
                                        queuetime2: moment(response.queueTime).add(30, 'Minutes').format('H:mm'),
                                        stationData: response,
                                    });
                                } else {
                                    this.setState({
                                        queuetime1: moment(response.queueTime).format('H:mm'),
                                        queuetime2: moment(response.queueTime).add(1, 'hour').format('H:mm'),
                                        stationData: response,
                                    });
                                }
                                if (this.state.setWhat === 'input') {
                                    this.setState({
                                        setWhat: response.status
                                    });
                                }
                                setTimeout(() => {
                                    this.setState({ step: 2, open: val });
                                }, 600);
                                this.hideBtn();
                            } else {
                                this.setState({ step: 3, open: val })
                            }
                        }
                    }).catch(
                        err => {
                            this.setState({ step: 3, open: val })
                            return err;
                        }
                    );
            }
        }
        else {
            this.setState({ open: val });
        }
    }
    async setOnservice() {
        sessionStorage.setItem('prevIndex', this.state.index);
        if (this.state.from === 'battery') {
            await spawBatteryOnservice(this.state.stationData.stationQueueId).then(
                response => {
                    this.setState({ step: 1 })
                    if (response.data) {
                        window.location.href = "/home_battery";
                    }
                }
            ).catch(
                err => {
                    this.setState({ step: 3 })
                    return err;
                }
            );
        } else {
            await maintenanceOnservice(this.state.stationData.stationQueueId).then(
                response => {
                    this.setState({ step: 1 })
                    if (response.data) {
                        window.location.href = "/home_service";
                    }
                }
            ).catch(
                err => {
                    this.setState({ step: 3 })
                    return err;
                }
            );
        }
    }
    async setFinishservice() {
        sessionStorage.setItem('prevIndex', this.state.index);
        if (this.state.from === 'battery') {
            await spawBatteryFinish(this.state.stationData.stationQueueId).then(
                response => {
                    this.setState({ step: 1 })
                    if (response.data) {
                        window.location.reload();
                    }
                }
            ).catch(
                err => {
                    this.setState({ step: 3 })
                    return err;
                }
            );
        } else {
            await maintenanceFinish(this.state.stationData.stationQueueId).then(
                response => {
                    this.setState({ step: 1 })
                    if (response.data) {
                        window.location.reload();
                    }
                }
            ).catch(
                err => {
                    this.setState({ step: 3 })
                    return err;
                }
            );
        }
    }
    async setConfirmService() {
        sessionStorage.setItem('prevIndex', this.state.index);
        if (this.state.from === 'battery') {
            await swapBatteryConfirm(this.state.stationData.stationQueueId).then(
                response => {
                    this.setState({ step: 1 })
                    if (response.data) {
                        window.location.reload();
                    }
                }
            ).catch(
                err => {
                    this.setState({ step: 3 })
                    return err;
                }
            );
        } else {
            await maintenanceConfirm(this.state.stationData.stationQueueId).then(
                response => {
                    this.setState({ step: 1 })
                    if (response.data) {
                        window.location.reload();
                    }
                }
            ).catch(
                err => {
                    this.setState({ step: 3 })
                    return err;
                }
            );
        }
    }
    async setRejectService() {
        sessionStorage.setItem('prevIndex', this.state.index);
        if (this.state.from === 'battery') {
            await swapBatteryReject(this.state.stationData.stationQueueId).then(
                response => {
                    this.setState({ step: 1 })
                    if (response.data) {
                        window.location.reload();
                    }
                }
            ).catch(
                err => {
                    this.setState({ step: 3 })
                    return err;
                }
            );
        } else {
            await maintenanceReject(this.state.stationData.stationQueueId).then(
                response => {
                    this.setState({ step: 1 })
                    if (response.data) {
                        window.location.reload();
                    }
                }
            ).catch(
                err => {
                    this.setState({ step: 3 })
                    return err;
                }
            );
        }
    }
    loadingScene() {
        return (
            <div className="row" >

                <div className="col-12 text-center" >
                    <img className="loadingPic"  src={loading_pic} alt="loading_pic" />
                </div>
            </div>
        );
    }
    hideBtn() {
        const temp1 = parseInt(localStorage.getItem('timeCurrentSelect'));
        const temp2 = parseInt(localStorage.getItem('timeTempSelect'));
        if (temp1 === temp2 || (temp1 + 1) === temp2) {
            this.setState({
                btnStyle: {
                    display: 'block'
                }
            })
        } else {
            this.setState({
                btnStyle: {
                    display: 'none'
                }
            })
        }
    }
    setEjectConfirm() {
        this.setState({ step: 3 })
    }
    setEjectReturn() {
        this.setState({ step: 2 })
    }
    queueSuccess() {
        var btn_;
        var btn_nd;
        var statusRender;
        if (this.state.setWhat === 'success') {
            statusRender = (<button className='btn-finish' style={{ fontSize: "12px" }}>
                บริการสำเร็จ
            </button>)
        } else if (this.state.setWhat === 'onservice') {
            statusRender = (<button className='btn-onservice' style={{ fontSize: "12px" }}>
                กำลังให้บริการ
            </button>)
            btn_ = (<button className="btn-main w-100" onClick={() => this.setFinishservice()} >
                เสร็จสิ้น
            </button>)
        } else if (this.state.setWhat === 'expired') {
            statusRender = (<button className='btn-expired' style={{ fontSize: "12px" }}>
                หมดอายุ
            </button>)
        } else if (this.state.setWhat === 'confirm') {
            statusRender = (<button className='btn-confirm' style={{ fontSize: "12px" }}>
                พร้อมให้บริการ
            </button>)
        }
        else if (this.state.setWhat === 'reject') {
            statusRender = (<button className='btn-expired' style={{ fontSize: "12px" }}>
                ยกเลิก
            </button>)
        } else if (this.state.setWhat === 'pending') {
            statusRender = (<button className='btn-pending' style={{ fontSize: "12px" }}>
                รอยืนยัน
            </button>)
            btn_ = (<button className="btn-main w-100" onClick={() => this.setConfirmService()} style={this.state.btnStyle}>
                พร้อมบริการ
            </button>)
            btn_nd = (<button className="btn-main-pending w-100" onClick={() => this.setEjectConfirm()} style={this.state.btnStyle}>
                ยกเลิกการจอง
            </button>)
        }
        return (
            <>
                <div className="row d-flex justify-content-center align-items-center" style={{ minHeight: '255px' }}>
                    <div className="col-lg-3 col-md-4 col-sm-5 col-6">
                        <span className="sub-title col-2">สถานี: </span><span className="detail">{this.state.stationData.station.name}</span><br />
                        <span className="sub-title col-2">จองไว้เวลา: </span><span className="detail">{this.state.queuetime1} - {this.state.queuetime2}</span><br />
                        {
                            this.state.from === 'battery' ?
                                <>  {
                                    this.state.stationData.contract == null ?
                                        <><span className="sub-title col-2">แพ็กเกจ: </span><span className="detail">ไม่มีแพ็กเกจ</span><br /></>
                                        :
                                        <><span className="sub-title col-2">แพ็กเกจ: </span><span className="detail">{this.state.stationData.contract.rentalPackageView}</span><br /></>
                                }</>
                                :
                                <><span className="sub-title">หมายเหตุ: </span><span className="detail">{this.state.stationData.note}</span><br /></>
                        }
                        <span className="sub-title col-2">คิวที่จอง: </span><span className="detail">{this.state.stationData.queueCode}</span><br />
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-5 col-6 d-flex justify-content-center align-items-center">
                        <div className="row m-0 p-0 w-100">
                            <div className="col-12  m-0 p-0">
                                <div className="row mb-2">
                                    <div className="col-12">
                                        <span className="sub-title-pending col-2">สถานะการบริการ </span>
                                        <br />
                                        {statusRender}
                                    </div>
                                </div>
                                {
                                    this.state.setWhat === 'pending' ?
                                        <> <div className="row mb-2">
                                            <div className="col-12 ">
                                                {btn_}
                                            </div>
                                        </div>
                                            <div className="row ">
                                                <div className="col-12">
                                                    {btn_nd}
                                                </div>
                                            </div></>
                                        :
                                        <> <div className="row mb-2">
                                            <div className="col-12 ">
                                                {btn_}
                                            </div>
                                        </div></>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
    confirmEject() {
        return (
            <>
                <div className="row d-flex justify-content-center align-items-center" style={{ minHeight: '255px' }}>
                    <div className="col-lg-3 col-md-4 col-sm-5 col-6 ">
                        <span className="sub-title col-2 ">
                            <img className="errorPic" src={error_icon} style={{ width: "15px" }} alt="error_icon" />
                        </span>
                        <span className="sub-title-long col-2 ">
                            ยืนยันยกเลิกการจอง
                        </span>
                        <br />
                        <span className="sub-title-long col-2">
                            และส่งข้อความแจ้งเตือน
                        </span>
                        <br />
                        <span className="fw-light sub-title col-2" value={' สล๊อตนี้จะเสียไป และจองไม่ได้อีกแจ้งเหตุผลกับศูนย์ใหญ่ทุกครั้งหลังการยกเลิก'}>
                            สล๊อตนี้จะเสียไป และจองไม่ได้อีกแจ้งเหตุผลกับศูนย์ใหญ่ทุกครั้งหลังการยกเลิก
                        </span>
                        <br />
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-5 col-6 d-flex justify-content-center align-items-center">
                        <div className="row m-0 p-0 w-100">
                            <div className="col-12  m-0 p-0">
                                <div className="row mb-2">
                                    <div className="col-12 ">
                                        <button className="btn-main w-100" onClick={() => this.setRejectService()} >
                                            ยืนยัน
                                        </button>
                                    </div>
                                </div>
                                <div className="row ">
                                    <div className="col-12 ">
                                        <button className="btn-main-pending w-100" onClick={() => this.setEjectReturn()} >
                                            ย้อนกลับ
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
    errorUI() {
        return (
            <>
                <div className="row" >
                    <div className="col-12" style={{ textAlign: "end" }}>
                        <img src={close} onClick={() => this.setOpen(false)} alt="close" />
                    </div>
                </div>
                <div className="row d-flex justify-content-center mb-2">
                    <div className="col-12 text-center">
                        <img className="errorPic" src={error_icon} alt="error_icon" />
                    </div>
                    <div className="col-12 text-center" style={{ marginTop: "5px" }}>
                        <span className="title-err">
                            ข้อมูลมีการผิดพลาด
                        </span>
                        <br />
                        <span className="sub-title">โปรดติดต่อเจ้าหน้าที่ศูนย์ใหญ่</span>
                    </div>
                    <div className="col-lg-3 col-md-4 col-sm-5 col-6 mt-2 text-center">
                        <button className="btn-main w-100" href="tel:0655098000">
                            ติดต่อเจ้าหน้าที่
                        </button>
                    </div>
                </div>
            </>
        );
    }
    render() {
        let comp;
        if (this.state.step === 1) {
            comp = this.loadingScene();
        } else if (this.state.step === 2) {
            comp = this.queueSuccess();
        } else if (this.state.step === 3) {
            comp = this.confirmEject();
        } else {
            comp = this.errorUI();
        }
        return (
            <>
                <button onClick={() => this.setOpen(true)} className="btn-next w-100" id="submitBtnSheet" >
                    ถัดไป
                </button>
                <BottomSheet open={this.state.open} onDismiss={() => this.setOpen(false)} blocking={false} snapPoints={({ minHeight, maxHeight }) => [276, 276]} style={{ "--rsbs-bg": "#f5f5f5" }}>
                    <div className="container" >
                        {comp}
                    </div>
                </BottomSheet>
            </>
        );
    }
}
export default BottomSheetx;