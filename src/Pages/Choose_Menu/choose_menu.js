import React, { Component } from "react";
import "../Welcome/welcome.css";
import logo from "../../img/logo_ETRAN.png";
import { stationNearby } from "../../services/station/station.js";
import { BottomSheet } from 'react-spring-bottom-sheet'
import close from "../../img/x1/close.png"
import error_icon from "../../img/x2/error_icon.png"

class ChooseMenu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            allowLocation: true,
            station: null,
            open: false
        };
        this.setOpen = this.setOpen.bind(this);
    }

    async componentDidMount() {
        if (localStorage.getItem('Access_')) {
            await navigator.geolocation.getCurrentPosition(function (position) {
                if (position) {
                    stationNearby(position.coords.latitude, position.coords.longitude).then(
                        result => {
                            sessionStorage.setItem('station', JSON.stringify(result.data[0]));
                        });
                }
            });
        }
        else {
            window.location.href = "/error";
        }
    }
    setOpen(val) {
        this.setState({ open: val });
    }
    batteryPage() {
        if (localStorage.getItem('Access_') && sessionStorage.getItem('station')) {
            window.location.href = "/home_battery";
        } else {
            setTimeout(() => {
                window.location.href = "/error";
            }, 1000);
        }
    }
    maintenancePage() {
        if (localStorage.getItem('Access_') && sessionStorage.getItem('station')) {
            let data = JSON.parse(sessionStorage.getItem('station'));
            if (data.stationId === '3b0b87b5-0ad2-4dc9-88b4-d44c9e666888' || data.stationId === '1da244f4-17b0-4d21-8e55-26c35e9cba81') {
                document.getElementById('errBtn').click();
            } else {
                window.location.href = "/home_service";
            }

        } else {
            setTimeout(() => {
                window.location.href = "/error";
            }, 1000);
        }
    }
    render() {
        return (
            <>

                {this.state.allowLocation ?
                    <div className="container-fluid content">
                        <div className="row">
                            <div className="col text-center mt-5">
                                <img className="logo" src={logo} alt="Logo" />
                            </div>
                        </div>
                        <div className="middle">
                            <div className="row">
                                <div className="col-6 text-center select-menu1">
                                    <span className="border_attach" onClick={this.batteryPage}>
                                        <div className="row d-flex justify-content-center" style={{ width: '100%' }}>
                                            <div className="col-12 text-center p-0">
                                                <p className="select-menu-font1">
                                                    เปลี่ยนแบต
                                                </p>
                                            </div>
                                            <div className="col-12 text-center p-0">
                                                <p className="select-menu-font2">
                                                    Battery Swap
                                                </p>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div className="col-6 text-center select-menu2">
                                    <span className="border_attach" onClick={this.maintenancePage}>
                                        <div className="row d-flex justify-content-center" style={{ width: '100%' }}>
                                            <div className="col-12 text-center p-0">
                                                <p className="select-menu-font1">
                                                    ซ่อมบำรุง
                                                </p>
                                            </div>
                                            <div className="col-12 text-center p-0">
                                                <p className="select-menu-font2">
                                                    Maintenance
                                                </p>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div className="col-12 mt-5 text-center">
                                    <h4 className="center-des2">โปรดเลือก หน้าหลักที่ต้องการจะใช้</h4>
                                </div>
                            </div>
                        </div>
                        <div className="row bottom">
                            <div className="col text-center">
                                <p style={{ fontFamily: "Prompt" }}>ขอให้เป็นวันที่ดีสำหรับคุณ :)</p>
                            </div>
                        </div>
                    </div>
                    :
                    <div className="container content">
                        <div className="row">
                            <div className="col text-center mt-5">
                                <img className="logo" src={logo} alt="Logo" />
                            </div>
                        </div>
                        <div className="middle">
                            <div className="row">
                                <div className="col-12 mt-5 text-center">
                                    <h4 className="center-des2">ไม่สามารถระบุ สถานที่</h4>
                                </div>
                            </div>
                        </div>
                        <div className="row bottom">
                            <div className="col text-center">
                                <p>ขอให้เป็นวันที่ดีสำหรับคุณ :)</p>
                            </div>
                        </div>
                    </div>
                }
                <button onClick={() => this.setOpen(true)} style={{ display: "none" }} id="errBtn">disable</button>
                <BottomSheet open={this.state.open} onDismiss={() => this.setOpen(false)} blocking={false} snapPoints={({ maxHeight }) => [276, 276]} style={{ backgroundColor: "white" }}>
                    <div className="container">
                        <div className="row">
                            <div className="col-12" style={{ textAlign: "end" }}>
                                <img src={close} onClick={() => this.setOpen(false)} alt="eclose_icon" />
                            </div>
                        </div>
                        <div className="row d-flex justify-content-center mb-2">
                            <div className="col-12 text-center">
                                <img className="errorPic" src={error_icon} alt="error_icon" />
                            </div>

                            <div className="col-12 text-center">
                                <span className="title-err">
                                    ขออภัยในความไม่สะดวก
                                </span>
                                <br />
                                <span className="sub-title">ปัจจุบันสถานีนี้ยังไม่เปิดทำการ</span>
                            </div>
                        </div>
                    </div>
                </BottomSheet>
            </>
        );
    }
}
export default ChooseMenu;