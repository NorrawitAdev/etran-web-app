import React, { Component } from "react";
import "./batteryInfo.css";
class Batteryinfo extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <div>
                <table className="table table-borderless text-center">
                    <thead>
                        <tr>
                            <th scope="col">รหัสแบตฯ</th>
                            <th scope="col">เหลือ (นาที)</th>
                            <th scope="col">กี่ (%)</th>
                            <th scope="col">สถานะแบต</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>321</td>
                            <td>0</td>
                            <td>100</td>
                            <td>
                                <span className="btn btn-info btn-ready">
                                    พร้อมใช้งาน
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>453</td>
                            <td>2</td>
                            <td>98</td>
                            <td>
                                <span className="btn btn-info btn-charge">
                                กำลังชาร์จ
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td>321</td>
                            <td>0</td>
                            <td>100</td>
                            <td>
                                <span className="btn btn-info btn-notCharge">
                                ชาร์จไม่เข้า
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}
export default Batteryinfo;