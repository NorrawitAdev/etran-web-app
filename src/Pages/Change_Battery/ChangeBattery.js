import React, { Component } from "react";
import "./Change_battery.css";
import Queue from "./Queue/queue.js";
class ChangeBattery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tab: 1,
            btSheet: 0
        };
        this.changeTab = this.changeTab.bind(this);
        this.openBottomSheet = this.openBottomSheet.bind(this);
    }
    changeTab(tabVal) {
        if (tabVal === 1) {
            document.getElementById("tab1").setAttribute("class", "btn btn-link underline active1");
            document.getElementById("tab2").setAttribute("class", "btn btn-link underline");
            this.setState({ tab: 1 });
        } else if (tabVal === 2) {
            document.getElementById("tab1").setAttribute("class", "btn btn-link underline");
            document.getElementById("tab2").setAttribute("class", "btn btn-link underline active1");
            this.setState({ tab: 2 });
        }
    }
    openBottomSheet(val) {
        this.setState({ btSheet: val });
    }
    render() {
        return (
            <div>
                <div className="row">
                    <div className="col">
                        <div className="btn-group" role="group">
                            <input type="radio" className="btn-check" name="btnradio" id="btnradio1" />
                            <label className="btn btn-link underline active1" id="tab1" onClick={() => this.changeTab(1)}>คิวของวันนี้</label>

                            <input type="radio" className="btn-check" name="btnradio" id="btnradio2" />
                            <label className="btn btn-link underline" id="tab2" onClick={() => this.changeTab(3)}>ข้อมูลแบตเตอรี่</label>
                        </div>
                    </div>
                </div>
                <Queue />
            </div>
        );
    }
}
export default ChangeBattery;