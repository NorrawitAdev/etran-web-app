import React, { Component } from "react";
import logo from "../../img/logo_ETRAN.png";
import backIcon from "../../img/x1.5/Left_Arrow.png";
import hashtag from "../../img/x1/hashtag.png";
import { InputNumberService } from "../../services/qrInputNumber";
import "./inputNumber.css";
class InputNumber extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    backPage() {
        window.history.back();
    }
    render() {
        return (
            <>
                <div className="container-fluid content">
                    <div className="row">
                        <div className="col-1 mt-5" style={{ paddingRight: "0", width: "25px", cursor: "pointer" }}>
                            <img className="back-page" src={backIcon} alt="backPage" onClick={this.backPage}/>
                        </div>
                        <div className="col text-center mt-5 pl-0" style={{ paddingLeft: "0" }}>
                            <img className="logo" src={logo} alt="Logo" />
                        </div>
                    </div>
                    <div className="row middle" style={{fontFamily:"Prompt"}}>
                        <div className="col-12">
                            <div className="row d-flex justify-content-center">
                                <div className="col-lg-5 col-md-7 col-sm-8 col-12 text-center">
                                    <span className="titleBorder">
                                        <label className="title">
                                            กรอกเลขการจอง
                                        </label>
                                        <img className="hash-tag" src={hashtag} alt="hashTag" />
                                    </span>
                                </div>
                            </div>
                            <><InputNumberService /></>
                            <div className="col-12 mt-3 text-center">
                                 <a  href="tel:0655098000" style={{justifyContent: "center",textDecoration:"none",color: "#202021"}}>ติดต่อศูนย์ใหญ่</a>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
export default InputNumber;