/* eslint-disable default-case */
import React, { Component } from "react";
import "./home.css";
import ChangeBattery from "../Change_Battery/ChangeBattery.js";
import Maintenance from "../Maintenance/maintenance.js";
function Page(props) {
  switch (props.page) {
    case "battery":
      localStorage.setItem("comeFrom", "battery");
      return <ChangeBattery />;
    case "service":
      localStorage.setItem("comeFrom", "service");
      return <Maintenance />;
  }
}
class Home extends Component {
  constructor(props) {
    super(props);
    if (this.props.toggle) {
      this.state = {
        params: this.props.params,
        toggle: this.props.toggle,
        styles: this.props.styles,
      };
    } else {
      this.state = {
        params: this.props.params,
        toggle: this.props.toggle,
        styles: this.props.styles,
      };
    }
  }
  componentDidUpdate(prevProps) {
    if (prevProps.toggle !== this.props.toggle) {
      if (this.props.toggle) {
        this.setState({
          params: this.props.params,
          toggle: this.props.toggle,
          styles: this.props.styles,
        });
      } else {
        this.setState({
          params: this.props.params,
          toggle: this.props.toggle,
          styles: this.props.styles,
        });
      }
    }
  }
  render() {
    return (
      <>
        <div className="topstyle">
          <div className="container borderR" style={this.state.styles.page}>
            <div className="row m-0 p-0">
              <div className="col m-0 p-0">
                <Page page={this.state.params} />
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default Home;
