import React from "react";
import "./topbar.css";
import QRCodeLogo from "../../img/x2/qr_code_logo.png"
import Home from "../Home/home.js";
import Hamburger from "../../img/x1/Hamburger_Menu.png";
import Battery_Swap_24px from "../../img/x1/Battery_Swap_24px.png";
import Maintenance_24px from "../../img/x1/Maintenance_24px.png";
import callout from "../../img/x1/callout.png";
import { CalendarOnService } from "../../services/calendar/CalendarOnService";
import {GetServiceStationQueueTop} from "../../services/firebaseDatabase/GetServiceStationQueue";
import {GetBatteryStationQueueTop} from "../../services/firebaseDatabase/GetBatteryStationQueue";
import calendarPicture from '../../img/calendar.png';
import calendarPicture_w from '../../img/calendar_w.png';
import { BottomSheet } from 'react-spring-bottom-sheet'
import moment from "moment";
import "moment/locale/th";
moment.locale('th');
class Topbar extends React.Component {
  constructor(props) {
    super(props);
   
    this.state = {
      calendarTop: new Date().getDate() + "/" + (new Date().getMonth() + 1) + "/" + new Date().getFullYear(),
      toggle: props.toggle,
      dateRender: new Date(),
      params: props.params,
      styles: {
        page: {
          transition: "all 0.5s ease 0s",
        },
      },
      station: {},
      menuBar: false,
      menuBarStyle: {
        padding: "0",
        background: "#F5F5F5",
        transition: "0.5s"
      },
      menuBarListStyle: {
        display: "none"
      }
    };
 
    this.onMenuBar = this.onMenuBar.bind(this);
    this.calendarOn = this.calendarOn.bind(this);
  }
  showSettings(event) {
    event.preventDefault();
  }
  componentDidMount() {
    const stationTemp = JSON.parse(sessionStorage.getItem('station'));
    this.setState({ station: stationTemp });
    if(sessionStorage.getItem('startDate') !== null) {
    this.setState({ calendarTop: moment(sessionStorage.getItem('startDate').replace(/["]+/g, '')).format('DD/MM/YYYY') });
    }
  }
  async onMenuBar() {
    if (this.state.menuBar === false) {
      this.setState({
        menuBar: true,
        menuBarStyle: {
          padding: "90px 0px 50px 250px",
          background: "transparent",
          transition: "0.5s",
          position: 'static',
        },
      });
      setTimeout(() => {
        this.setState({
          menuBarListStyle: {
            width: "200px",
            transition: "1s"
          }
        });
      }, 400);
    } else {
      this.setState({
        menuBar: false,
        menuBarStyle: {
          padding: "0",
          background: "#F5F5F5",
          transition: "0.5s"
        },
        menuBarListStyle: {
          display: "none"
        }
      });
    }
  }
  calendarOn(val) {
    this.setState({
      open: val
    })
  }
  setHover(val) {
    this.setState({
      hoverOn: val
    })
  }
  render() {
    var hover
    if (this.state.hoverOn === true) {
      hover = <img className="transparent ms-2 mb-1 " src={calendarPicture_w} alt="calendarPicture_w" />
    } else {
      hover = <img className="transparent ms-2 mb-1 " src={calendarPicture} alt="calendarPicture" />
    }


    return (
      <div className="card-body" style={this.state.menuBarStyle}>
        <div className="sideBar" style={this.state.menuBarListStyle}>
          <div className="row m-0 p-0">
            <div className="col-12 text-center list-color">
              <img className="station-pic" src={this.state.station.imageUrl} alt="station-pic" />
            </div>
            <div className="col-12 text-title-sideBar list-color">
              สถานี: {this.state.station.name}
            </div>
            <div className="col-12 list-color">
              {
                this.props.params === "battery" ?
                  <span className="text-subtitle-sideBar list-color">เวลาเปิด: 24 ชั่วโมง</span>
                  :
                  <span className="text-subtitle-sideBar list-color">เวลาเปิด: 9:00 - 17:00</span>
              }
            </div>
          </div>
          <div className="row m-0 p-0">
            <div className="col-3 list-color side-height">
              <a className="text-subtitle-sideBar list-color" href="home_battery">
                <img className="sidebar-icon" src={Battery_Swap_24px} alt="sidebar-icon" />
              </a>
            </div>
            <div className="col-9 list-color side-right side-height">
              <a className="text-subtitle-sideBar list-color" href="home_battery">
                เปลี่ยนแบต
              </a>
            </div>
          </div>
          <div className="row m-0 p-0">
            <div className="col-3 list-color side-height">
              <a className="text-subtitle-sideBar list-color" href="home_service">
                <img className="sidebar-icon" src={Maintenance_24px} alt="sidebar-icon" />
              </a>
            </div>
            <div className="col-9 list-color side-right side-height">
              <a className="text-subtitle-sideBar list-color" href="home_service">
                เข้าซ่อมบำรุง
              </a>
            </div>
          </div>
          <div className="row m-0 p-0">
            <div className="col-3 list-color side-height">
              <a className="text-subtitle-sideBar list-color" href="/">
                <img className="sidebar-icon" src={callout} alt="sidebar-icon" />
              </a>
            </div>
            <div className="col-9 list-color side-right side-height">
              <a className="text-subtitle-sideBar list-color" href="tel:0655098000" style={{ justifyContent: "center", textDecoration: "none" }}>โทรหาศูนย์ใหญ่</a>
            </div>
          </div>
        </div>
        <div className="allBorder">
          <div className="row d-flex justify-content-between p-0 m-0 top_bar" style={{ fontFamily: "Prompt" }}>
            <div className="col-sm-1 col-2 p-0 m-0 d-flex justify-content-center" style={{ background: "transparent", alignItems: "center" }}>
              <img className="hamburger" src={Hamburger} onClick={this.onMenuBar} alt="Hamburger" />
            </div>
            <div className="col m-top" style={this.state.styles.page}>
              <div className="row">
                <div className="col-12 pr-0">
                  <span className="station">สถานี: {this.state.station.name}</span>
                </div>
                <div className="col-12 pr-0 mr-0">
                  <span>
                    สถานะ:
                    {
                      this.state.station.status === true ?
                        <>พร้อมบริการ<span className="dot"></span></>
                        :
                        <>ไม่พร้อมบริการ<span className="dot_red"></span></>
                    }
                  </span>
                </div>
              </div>
            </div>
            <div className="col m-top col-d-flex" style={{ paddingRight: "50px" }}>
              รอรับบริการ
              {
                this.props.params === "battery" ?
                  < GetBatteryStationQueueTop />
                  :
                  < GetServiceStationQueueTop />
              }
            </div>
            {
               this.props.params !== "battery" ?
               <div className="col-3 m-top col-d-flex" style={{ paddingRight: "50px" }}>
               <button className="btn btn-outline-dark" onClick={() => this.calendarOn(true)} onMouseOver={() => this.setHover(true)} onMouseOut={() => this.setHover(false)}>
                 {/* {new Date().getDate()}/{new Date().getMonth() + 1}/{new Date().getFullYear()}  */}
                 {this.state.calendarTop}
                 {hover}
               </button>
             </div>
              :
                <></>
            }
           
            <BottomSheet open={this.state.open} blocking={false} onDismiss={() => this.calendarOn(false)} snapPoints={({ minHeight }) => minHeight} style={{ backgroundColor: "white" }}>
              <div className="container" >
                
                <CalendarOnService />
              </div>
            </BottomSheet>
            <div className="col-1 m-top col-d-flex" style={{ paddingRight: "50px" }} >
              <a href="/qr">
                <img className="Vector_logo" src={QRCodeLogo} alt="QRCodeLogo" />
              </a>
            </div>
          </div>
          <Home params={this.state.params} toggle={this.state.toggle} styles={this.state.styles} />
        </div>
      </div>
    );
  }
}
export default Topbar;
