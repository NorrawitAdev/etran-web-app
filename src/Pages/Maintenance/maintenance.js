import React, { Component } from "react";
import "./maintenance.css";
import {ServiceMaintenance} from "../../services/maintenanceService/ServiceMaintenance";
import { ServiceMaintenanceList } from "../../services/maintenanceService/ServiceMaintenanceList";
import {GetServiceStationQueue} from "../../services/firebaseDatabase/GetServiceStationQueue";
class Maintenance extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    render() {
        return (
            <><GetServiceStationQueue/></>
        );
    }
}
export default Maintenance;