import React from 'react';
import moment from 'moment';
import "moment/locale/th";
import 'react-spring-bottom-sheet/dist/style.css'
import "../../Pages/Bottom_Sheet/bottom_sheet.css";
import BottomSheetx from "../../Pages/Bottom_Sheet/bottom_sheet.js"
moment.locale('th');
class InputNumberService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userGuest: null,
            nameStation: [],
            queueService: [],
            queueBooking: [],
            stationQueue: [],
            stationQueueId: [],
            value: '',
            form: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }
    async componentDidMount() {
        document.getElementById('submitBtnSheet').setAttribute("disabled", 'true');
        document.getElementById('submitBtnSheet').setAttribute("style", "background-color:#dcdcdc;");
    }
    handleChange(event) {
        this.setState({ value: event.target.value.toUpperCase() });
        this.setState({ form: "get" });
        if (event.target.value !== '') {
            document.getElementById('submitBtnSheet').removeAttribute('disabled');
            document.getElementById('submitBtnSheet').removeAttribute('style');
        } else {
            document.getElementById('submitBtnSheet').setAttribute("disabled", 'true');
            document.getElementById('submitBtnSheet').setAttribute("style", "background-color: #dcdcdc;");
        }
    }
    render() {
        return (
            <>
                <div className="row d-flex justify-content-center" style={{ marginTop: "45px" }}>
                    <div className="col" style={{ maxWidth: "303px" }}>
                        <label className="text-on-input">กรุณากรอกเลขเพื่อไปต่อ</label>
                        <input type="text" value={this.state.value} onChange={this.handleChange} className="form-control input-style" />
                    </div>
                </div>
                <div className="row d-flex justify-content-center">
                    <div className="col-12 mt-3" style={{ maxWidth: "240px" }}>
                        <BottomSheetx stationQueueId={this.state.value} setWhat="input" />
                    </div>
                </div>
            </>
        );
    }
}
export { InputNumberService }; 