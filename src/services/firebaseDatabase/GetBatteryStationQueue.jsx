import React from 'react';
import { database, getAuth, signInAnonymously, ref,  onValue } from '../../Pages/firebase.js';
import { SwapBatteryService } from '../swapBatteryService/SwapBatteryService';
import { TopBarSwapBattery } from '../topBar/TopBarSwapBattery';

const auth = getAuth();
signInAnonymously(auth)


class GetBatteryStationQueue extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
        };

    }
    componentDidMount() {
        const stationTemp = JSON.parse(sessionStorage.getItem('station'));
        const queueStatus = ref(database, 'station/swap/' + stationTemp['stationId']);
        onValue(queueStatus, (snapshot) => {
            const data = snapshot.val();
        
            this.setState({
                data: data
            });
        });
    }
    componentWillUnmount() {

    }


    render() {

        return (
            <div>
                <SwapBatteryService data={this.state.data} />
            </div>

        );
    }
}
class GetBatteryStationQueueTop extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
        };

    }
    componentDidMount() {
        const stationTemp = JSON.parse(sessionStorage.getItem('station'));
        const queueStatus = ref(database, 'station/swap/' + stationTemp['stationId']);
        onValue(queueStatus, (snapshot) => {
            const data = snapshot.val();
    
            this.setState({
                data: data
            });
        });
    }
    componentWillUnmount() {

    }


    render() {

        return (
            <div>
                <TopBarSwapBattery data={this.state.data} />
            </div>

        );
    }
}



export { GetBatteryStationQueue,GetBatteryStationQueueTop }; 