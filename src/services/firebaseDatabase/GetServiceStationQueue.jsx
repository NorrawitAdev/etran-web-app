import React from 'react';

import { database, getAuth, signInAnonymously, ref, onValue } from '../../Pages/firebase.js';
import { ServiceMaintenanceList } from '../maintenanceService/ServiceMaintenanceList';
import { TopBarMaintenance } from '../topBar/TopBarMaintenance';
const auth = getAuth();
signInAnonymously(auth)

class GetServiceStationQueue extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
        };
    }
    componentDidMount() {
        const stationTemp = JSON.parse(sessionStorage.getItem('station'));
        const queueStatus = ref(database, 'station/maintenance/' + stationTemp['stationId']);
        onValue(queueStatus, (snapshot) => {
            const data = snapshot.val();
        
            this.setState({
                data: data
            });
        });
    }
    render() {
        return (
            <div>
                <ServiceMaintenanceList data={this.state.data} />
            </div>
        );
    }
}

class GetServiceStationQueueTop extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: null,
        };
    }
    componentDidMount() {
        const stationTemp = JSON.parse(sessionStorage.getItem('station'));
        const queueStatus = ref(database, 'station/maintenance/' + stationTemp['stationId']);
        onValue(queueStatus, (snapshot) => {
            const data = snapshot.val();
         
            this.setState({
                data: data
            });
        });
    }
    render() {
        return (
            <div>
                <TopBarMaintenance data={this.state.data} />
            </div>
        );
    }
}
export { GetServiceStationQueue,GetServiceStationQueueTop }; 