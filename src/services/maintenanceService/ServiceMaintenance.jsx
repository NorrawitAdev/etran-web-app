import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import { ServiceMaintenanceList } from "./ServiceMaintenanceList"
import { getMaintenanceStationQueue, getMaintenanceStationQueueDetail } from './maintenanceAPI.js'
import { SwiperSlide } from "swiper/react";
import SwiperCore, {
    Pagination,
 
} from 'swiper';
import "swiper/css";
import './style.css'
import moment from 'moment';
import "moment/locale/th";
moment.locale('th');
SwiperCore.use([Pagination]);
let timeQueue = [];
let hourPlus = [];
let unixTime = [];
let swiper = ''
let hourPlusUnix = [];
let timeLocalNow = moment().format();
let subLocalUnix = moment(timeLocalNow).subtract(61, 'Minutes').unix(); //Maybe 60
let timeLocalNowUnix = moment(timeLocalNow).unix();

class ServiceMaintenance extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userGuest: null,
            nameStation: [],
            queueService: [],
            queueList: [],
            textNomal: 'text-btn-time',
            textUse: 'text-btn-time2',
            btnNomal: 'btn btn-time',
            textDisable: 'text-btn-time opacity',
            btnDisable: 'btn btn-time opacity',
            btnUse: 'btn btn-time btn-use',
            open: false,
            step: 1,
            timeCurrentSelect: '',
            timeSelect: '',
            timeItem: [],
            detailItem: [],
            carousel: '',
            perView: '',
            slideProgress: '',
            tempTimeData: [],
            queueServiceUpdate: [],
            queueListUpdate: [],
            timeSelectUpdate: '',

        };
        this.setDefualtTimeText = this.setDefualtTimeText.bind(this);
        this.setDefualtTimeBtn = this.setDefualtTimeBtn.bind(this);
        this.getDetail = this.getDetail.bind(this);
        this.setCarousel = this.setCarousel.bind(this);
        this.getWindowDimensions = this.getWindowDimensions.bind(this);
        this.getDataTime = this.getDataTime.bind(this);
    }
    async componentDidMount() {
       
        this.getWindowDimensions();
        document.getElementById('submitBtnSheet').setAttribute("style", "display:none");
        swiper = document.querySelector('.swiper').swiper;
        const stationTemp = JSON.parse(sessionStorage.getItem('station'));
        this.setState({ nameStation: stationTemp })
        await getMaintenanceStationQueue(stationTemp['stationId']).then(
            response => {
                this.setState({ queueService: response.data })
            }
        );
        this.getDataTime()
       
    }
    componentWillUnmount(){
       
    }
    getDataTime() {
        let timeItems = [];
        let queueItems = [];
        let rowItem = [];
        let date = new Date();
        const day = String(date.getDate()).padStart(2, '0');
        this.state.queueService.splice(3, 1); 
        for (let i = 0; i < this.state.queueService.length; i++) {
            if (moment(this.state.queueService[i].queueTime).format('DD') === day) {
       
                unixTime = moment(this.state.queueService[i].queueTime).unix();
                hourPlus = moment(this.state.queueService[i].queueTime).add(60, 'Minutes').format('H:mm');
                hourPlusUnix = moment(this.state.queueService[i].queueTime).add(59, 'Minutes').unix();
                timeQueue = moment(this.state.queueService[i].queueTime).format('H:mm');
                timeItems = <span key={uuidv4()} className={this.setDefualtTimeText()} id={'span1_' + i}>{timeQueue + ' '
                    + '- ' + hourPlus}</span>;
                queueItems = <span key={uuidv4()} className={this.setDefualtTimeText()} id={'span2_' + i}>{(this.state.queueService[i].numberOfQueue) + ' คัน'}</span>;
                const temp = (
                    <SwiperSlide key={i} id='slider'>
                        <button className={this.setDefualtTimeBtn(i)} key={uuidv4()} id={'time_' + i} onClick={() => this.getDetail(this.state.queueService[i].queueTime, i)}>
                            {timeItems}
                            <br />
                            <span >
                                {queueItems}
                            </span>
                        </button>
                    </SwiperSlide>
                );
                rowItem.push(temp);
            }
        }
        this.setState({ timeItem: rowItem });
        this.setCarousel();
        const prevIndex = sessionStorage.getItem('prevIndex');
        const prevTime = sessionStorage.getItem('prevTime');
        if (prevIndex || prevIndex != null) {
            document.getElementById('time_' + prevIndex).click();
            localStorage.setItem('timeCurrentSelect', prevIndex);
            localStorage.setItem('timeTempSelect', prevIndex);
            sessionStorage.removeItem('prevIndex');
            sessionStorage.removeItem('prevTime');
        } else if (prevTime || prevTime != null) {
            for (let i = 0; i < this.state.tempTimeData.length; i++) {
                if (this.state.tempTimeData[i]['time'] === parseInt(prevTime)) {
                    document.getElementById('time_' + this.state.tempTimeData[i]['index']).click();
                    localStorage.setItem('timeCurrentSelect', this.state.tempTimeData[i]['index']);
                    localStorage.setItem('timeTempSelect', this.state.tempTimeData[i]['index']);
                    break;
                }
            }
        }
        else if (this.state.timeCurrentSelect !== '') {
            document.getElementById('time_' + this.state.timeCurrentSelect).click();
            localStorage.setItem('timeCurrentSelect', this.state.timeCurrentSelect);
            localStorage.setItem('timeTempSelect', this.state.timeCurrentSelect);
        }
    }
    async getDetail(queueTime, index) {
        const startDate = moment(queueTime).format('YYYY-MM-DD HH:00:00');
        const endDate = moment(queueTime).format('YYYY-MM-DD HH:30:00');
        const dayNow = moment().format('YYYY-MM-DD') + ' 10:00:00'; 
        const dayNowUnix = moment(dayNow).unix(); 
        await getMaintenanceStationQueueDetail(this.state.nameStation.stationId, startDate, endDate).then(
            response => {
                this.setState({ queueList: response.data, timeSelect: startDate })
            }
        );
      
        for (let i = 0; i < this.state.timeItem.length; i++) {
            if (i === index) {
                document.getElementById('span1_' + i).setAttribute("class", "text-btn-time2")
                document.getElementById('span2_' + i).setAttribute("class", "text-btn-time2")
                document.getElementById('time_' + i).setAttribute("class", "btn btn-time btn-use")
            } else {
                if (i < this.state.timeCurrentSelect) {
                    document.getElementById('span1_' + i).setAttribute("class", "text-btn-time opacity")
                    document.getElementById('span2_' + i).setAttribute("class", "text-btn-time opacity")
                    document.getElementById('time_' + i).setAttribute("class", "btn btn-time opacity")
                }
                else if (timeLocalNowUnix < dayNowUnix) {
                    document.getElementById('span1_0').setAttribute("class", "text-btn-time")
                    document.getElementById('span2_0').setAttribute("class", "text-btn-time")
                    document.getElementById('time_0').setAttribute("class", "btn btn-time")
                } 
                else if (timeLocalNowUnix > unixTime) {
                    document.getElementById('span1_' + i).setAttribute("class", "text-btn-time opacity")
                    document.getElementById('span2_' + i).setAttribute("class", "text-btn-time opacity")
                    document.getElementById('time_' + i).setAttribute("class", "btn btn-time opacity")
                }
                else {
                    document.getElementById('span1_' + i).setAttribute("class", "text-btn-time")
                    document.getElementById('span2_' + i).setAttribute("class", "text-btn-time")
                    document.getElementById('time_' + i).setAttribute("class", "btn btn-time")
                }
            }
        }
        localStorage.setItem('timeTempSelect', index);
        localStorage.setItem('timeCurrentSelect', index);
    }
    setDefualtTimeText() {
        if (unixTime <= timeLocalNowUnix && hourPlusUnix >= timeLocalNowUnix) {
            return this.state.textUse;
        }
        else if (unixTime < subLocalUnix) {
            return this.state.textDisable;
        }
        else {
            return this.state.textNomal;
        }
    }
    setDefualtTimeBtn(index) {
        this.state.tempTimeData.push({
            time: unixTime,
            index: index
        });
        if (unixTime <= timeLocalNowUnix && hourPlusUnix >= timeLocalNowUnix) {
            this.setState({ timeCurrentSelect: index });
            swiper.slideTo(index);
            return this.state.btnUse;
        } else if (unixTime < subLocalUnix) {
            return this.state.btnDisable;
        } else {
            return this.state.btnNomal;
        }
    }
    async getWindowDimensions() {
        const { innerWidth: width, innerHeight: height } = window;
        if (width === 2000 && height === 1200) {
            this.setState({
                perView: 7
            })
        } else if (width === 1200 && height === 2000) {
            this.setState({
                perView: 6
            })

        } else {
            this.setState({
                perView: 4,
                slideProgress: 'true'
            })
        }
    }
    setCarousel() {
        if (this.state.timeItem) {
            let timeItemOne = this.state.timeItem[0];
            if (timeItemOne.props.id === 'time_0') {
                this.setState({
                    carousel: 'carousel-item active '
                })
            } else {
                this.setState({
                    carousel: 'carousel-item'
                })
            }
        }
    }
    render() {
        return (
            <>
               
                <ServiceMaintenanceList queueList={this.state.queueList} selectTime={this.state.timeSelect} />
            </>
        );
    }
}
export { ServiceMaintenance }; 