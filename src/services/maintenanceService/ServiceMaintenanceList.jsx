import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import "moment/locale/th";
import BottomSheetx from "../../Pages/Bottom_Sheet/bottom_sheet.js";
import { getMaintenanceStationQueue, getMaintenanceStationQueueDetail } from './maintenanceAPI.js'
import loading_pic from "../../img/loading.gif"
import moment from 'moment';
import "moment/locale/th";
moment.locale('th');
const stationTemp = JSON.parse(sessionStorage.getItem('station'));
class ServiceMaintenanceList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userGuest: null,
            nameStation: [],
            queueService: [],
            queueList: [],
            selectTime: 0,
            step:1,
            doing: '',
            startDate: moment().format('YYYY-MM-DD 00:00:00'),
            endDate: moment().format('YYYY-MM-DD 24:00:00'),
            startDateString: moment().format('YYYY-MM-DD 00:00:00'),
            endDateString: moment().format('YYYY-MM-DD 24:00:00'),
            startDateUse: moment().format('YYYY-MM-DD 00:00:00'),
            endDateUse: moment().format('YYYY-MM-DD 24:00:00'),
        };
        this.getDetail = this.getDetail.bind(this);
        this.loadingScene = this.loadingScene.bind(this);
    }
    async componentDidMount() {
        if(localStorage.getItem('Access_')){
            if(sessionStorage.getItem('startDate') !== null) {
                this.setState({ startDateString: sessionStorage.getItem('startDate').replace(/["]+/g, ''), endDateString: sessionStorage.getItem('endDate').replace(/["]+/g, '') })
                }
                this.setState({ nameStation: stationTemp })
                await getMaintenanceStationQueue(stationTemp['stationId']).then(
                    response => {
                        this.setState({ queueService: response.data,step:2 })
                        this.getDetail(this.state.startDateString,this.state.endDateString)
                    }
                );
                document.getElementById('submitBtnSheet').setAttribute("style", "display:none"); 
        }else{
            window.location.href = "/error";
        }
    }
    loadingScene() {
        return (
            <>
                <div className="container-fluid" >

                    <div className="col-12 text-center" >
                        <img className="loadingPic" src={loading_pic} alt="loading_pic" />
                    </div>
                </div></>

        );
    }
    async getDetail(startDate,endDate) {
        await getMaintenanceStationQueueDetail(this.state.nameStation.stationId, startDate, endDate).then(
            response => {
                this.setState({ queueList: response.data })
            }
        );    
    }
    selectService(status, stationQueueId) {
        var doWhat;
        switch (status) {
            case 'pending':
                doWhat = 'pending'
                break;
            case 'onservice':
                doWhat = 'onservice'
                break;
            case 'expired':
                doWhat = 'expired'
                break;
            case 'finish':
                doWhat = 'finish'
                break;
            case 'cancel':
                doWhat = 'cancel'
                break;
            case 'reject':
                doWhat = 'reject'
                break;
            case 'success':
                doWhat = 'success'
                break;
            case 'confirm':
                doWhat = 'confirm'
                break;
            default:
        }
        this.setState({ stationQueueIdSelect: stationQueueId, doing: doWhat })
        setTimeout(() => {
            document.getElementById("submitBtnSheet").click();
        }, 200);
    }
    async componentDidUpdate(prevProps) {
        if(prevProps.data !== this.props.data){
        
            await getMaintenanceStationQueue(stationTemp['stationId']).then(
                response => {
                    this.setState({ queueService: response.data })
                }
            );
            this.getDetail(this.state.startDateString,this.state.endDateString)
        }
    }
    render() {
        let listItems = [];
        for (let i = 0; i < this.state.queueList.length; i++) {
            if (this.state.queueList.length > 0) {
                var status;
                if (this.state.queueList[i].status === 'onservice') {
                    status = (<button className='btn-onservice' >
                        กำลังให้บริการ
                    </button>);
                } else if (this.state.queueList[i].status === 'expired') {
                    status = (<button className='btn-expired'>
                        หมดอายุ
                    </button>);
                } else if (this.state.queueList[i].status === 'success') {
                    status = (<button className='btn-finish'>
                        บริการสำเร็จ
                    </button>);
                } else if (this.state.queueList[i].status === 'confirm') {
                    status = (<button className='btn-confirm'>
                        พร้อมบริการ
                    </button>);
                } else if (this.state.queueList[i].status === 'pending') {
                    status = (<button className='btn-pending'>
                        รอยืนยัน
                    </button>);
                } else if (this.state.queueList[i].status === 'reject') {
                    status = (<button className='btn-expired'>
                        ยกเลิก
                    </button>);
                }
                listItems.push(
                    <tr key={uuidv4()} onClick={() => this.selectService(this.state.queueList[i].status, this.state.queueList[i].stationQueueId)} style={{ cursor: "pointer" }}>
                        <td>
                            {(moment(this.state.queueList[i].queueTime).format('HH:mm'))}
                        </td>
                        <td> {(this.state.queueList[i].vehicleNo)}</td>
                        <td>
                            {status}
                        </td>
                    </tr>)
            }
        }
        var res ;
        if(this.state.step === 1){
            res = <>{this.loadingScene()}</>
        }else{
            res = <>
            <table className="table table-borderless text-center">
                <thead>
                    <tr>
                        <th scope="col">เวลาจอง</th>
                        <th scope="col">ทะเบียนรถ</th>
                        <th scope="col">สถานะการจอง</th>
                    </tr>
                </thead>
                <tbody>
                    {listItems}
                </tbody>
            </table>

            <BottomSheetx stationQueueId={this.state.stationQueueIdSelect} setWhat={this.state.doing} />
        </>
        }
        return (
            <>
              {res}
            </>
        );
    }
}
export { ServiceMaintenanceList }; 