import axios from 'axios';

export async function maintenanceOnservice(stationQueueId) {
    const token = localStorage.getItem('Access_');
    const body = {};
    const response = await axios.post(process.env.REACT_APP_API_URL + 'api/service-maintenance/onservice/' + stationQueueId + '?access_token=' + token, body);
    return response;
}

export async function maintenanceFinish(stationQueueId) {
    const token = localStorage.getItem('Access_');
    const body = {};
    const response = await axios.post(process.env.REACT_APP_API_URL + 'api/service-maintenance/finish/' + stationQueueId + '?access_token=' + token, body);
    return response;
}

export async function getMaintenanceStationQueue(stationId) {
    const token = localStorage.getItem('Access_');
    const response = await axios.get(process.env.REACT_APP_API_URL + 'api/service-maintenance/station-queue/' + stationId + '?access_token=' + token);
    return response;
}

export async function getMaintenanceStationQueueDetail(stationId, startDate, endDate) {
    const token = localStorage.getItem('Access_');
    const response = await axios.get(process.env.REACT_APP_API_URL + 'api/station-queue?stationId=' + stationId + '&service=service&startDate=' + startDate + '&endDate=' + endDate + '&access_token=' + token);
    return response;
}

export async function getMaintenanceStationQueueAllDetail(stationId) {
    const token = localStorage.getItem('Access_');
    const response = await axios.get(process.env.REACT_APP_API_URL + 'api/station-queue?stationId=' + stationId + '&service=service&access_token=' + token);
    return response;
}

export async function maintenanceConfirm(stationQueueId) {
    const token = localStorage.getItem('Access_');
    const body = {};
    const response = await axios.post(process.env.REACT_APP_API_URL + 'api/service-maintenance/confirm/' + stationQueueId + '?access_token=' + token, body);
    return response;
}

export async function maintenanceReject(stationQueueId) {
    const token = localStorage.getItem('Access_');
    const body = {};
    const response = await axios.post(process.env.REACT_APP_API_URL + 'api/service-maintenance/reject/' + stationQueueId + '?access_token=' + token, body);
    return response;
}


