import axios from 'axios';

export async function stationNearby(lat, lon) {
    const token = localStorage.getItem('Access_');
    // const body = { service: 'all', location: "13.772219,100.54119740000002" };
    const body = { service: 'all', location: lat + ',' + lon };
    const response = await axios.post(process.env.REACT_APP_API_URL + 'api/stations/nearby?access_token=' + token, body);
    return response;
}

export async function stationQueueDetail(stationQueueId) {
    const token = localStorage.getItem('Access_');
    const response = await axios.get(process.env.REACT_APP_API_URL + 'api/station-queue/' + stationQueueId + '?access_token=' + token);
    return response;
}

