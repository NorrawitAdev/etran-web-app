import React from 'react';
import Calendar from 'react-calendar';
import { getMaintenanceStationQueue, getMaintenanceStationQueueDetail } from './maintenanceAPI.js'
import { ServiceMaintenanceList } from '../maintenanceService/ServiceMaintenanceList.jsx';
import Topbar from '../../Pages/Topbar/topbar'
import { differenceInCalendarDays } from 'date-fns';
import './style.css'
import moment from 'moment';
import "moment/locale/th";
moment.locale('th');


class CalendarOnService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            view: 'month',
            dateUse: new Date(),
            value: '',


        };
        this.onChange = this.onChange.bind(this);

    }
    onChange = date => this.setState({ date })
    async componentDidMount() {
        if (sessionStorage.getItem('startDate') !== null) {
            this.setState({ dateUse: new Date(sessionStorage.getItem('startDate').replace(/["]+/g, '')) })
        }

        if (sessionStorage.getItem('value') !== null) {
            this.setState({ value: new Date(sessionStorage.getItem('value')) })
        }
       
       

    }
    async onClickDay(value, event) {
        // do something
        let startDate = moment(value).format('YYYY-MM-DD HH:mm:ss');
        sessionStorage.setItem('value', moment(value).format('YYYY-MM-DD HH:mm:ss'));
        sessionStorage.setItem('startDate', JSON.stringify(startDate))
        let endDate = moment(value).format('YYYY-MM-DD 24:mm:ss');
        sessionStorage.setItem('endDate', JSON.stringify(endDate))

        window.location.reload();
    }
    async onClickMonth(value, event) {
        let startDate = moment(value).format('YYYY-MM-DD HH:mm:ss');
        sessionStorage.setItem('month',JSON.stringify(startDate));

    }
  
    render() {
        return (
            <>
                <Calendar
                    onChange={this.onChange}
                    value={this.state.dateUse}
                    onClickDay={this.onClickDay}
                    onClickMonth={this.onClickMonth}
                    // activeStartDate={this.state.date}

                />
                <div className="container">
                </div>
            </>
        );
    }
}
export { CalendarOnService }; 