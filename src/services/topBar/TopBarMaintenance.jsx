import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import moment from 'moment';
import "moment/locale/th";
import { getMaintenanceStationQueueAllDetail } from '../maintenanceService/maintenanceAPI.js'
import { database, getAuth, signInAnonymously, ref, onValue } from '../../Pages/firebase.js';
const stationTemp = JSON.parse(sessionStorage.getItem('station'));
const auth = getAuth();
signInAnonymously(auth)
class TopBarMaintenance extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userGuest: null,
            nameStation: [],
            queueService: [],
            queueBooking: [],
            stationQueue: [],
            dateUse: new Date(),
            data: ''
        };

    }
    async componentDidMount() {
        const queueStatus = ref(database, 'station/maintenance/' + stationTemp['stationId']);
        onValue(queueStatus, (snapshot) => {
            const data = snapshot.val();
        
            this.setState({
                data: data
            });
        });
        if (sessionStorage.getItem('startDate') !== null) {
            this.setState({ dateUse: new Date(sessionStorage.getItem('startDate').replace(/["]+/g, '')) })
        }

        this.setState({ nameStation: stationTemp })
        await getMaintenanceStationQueueAllDetail(stationTemp['stationId']).then(
            response => {
                this.setState({ stationQueue: response.data })
            }
        );
      

    }
    async componentDidUpdate(prevProps) {
        if (prevProps.data !== this.props.data) {
          
            await getMaintenanceStationQueueAllDetail(stationTemp['stationId']).then(
                response => {
                    this.setState({ stationQueue: response.data })
                }
            );
        }
    }
    render() {
        moment.locale('th');
        const { stationQueue } = this.state;
        const queueItems = [];
        let date = this.state.dateUse
        const day = String(date.getDate()).padStart(2, '0');
        const countQueuePending = stationQueue.filter(item => item.status === 'pending' && moment(item.queueTime).format('DD') === day),
            queuePendingCount = countQueuePending.length;
        queueItems.push(<span key={uuidv4()} className="vehicleValue">{queuePendingCount + ' คัน'}</span>)
        return (
            <>
                {queueItems}
            </>
        );
    }
}
export { TopBarMaintenance }; 