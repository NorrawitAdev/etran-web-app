import axios from 'axios';

export async function loginGuest_API() {
    const response = await axios.post(process.env.REACT_APP_API_URL + 'api/users/login-guest', '{}');
    return response;
}