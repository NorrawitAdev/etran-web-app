import axios from 'axios';

export async function spawBatteryOnservice(stationQueueId) {
    const token = localStorage.getItem('Access_');
    const body = {};
    const response = await axios.post(process.env.REACT_APP_API_URL + 'api/swap-battery/onservice/' + stationQueueId + '?access_token=' + token, body);
    return response;
}

export async function spawBatteryFinish(stationQueueId) {
    const token = localStorage.getItem('Access_');
    const body = {};
    const response = await axios.post(process.env.REACT_APP_API_URL + 'api/swap-battery/finish/' + stationQueueId + '?access_token=' + token, body);
    return response;
}

export async function getSpawStationQueue(stationId) {
    const token = localStorage.getItem('Access_');
    const response = await axios.get(process.env.REACT_APP_API_URL + 'api/swap-battery/station-queue/' + stationId + '?access_token=' + token);
    return response;
}

export async function getSpawStationQueueDetail(stationId, startDate, endDate) {
    const token = localStorage.getItem('Access_');
    const response = await axios.get(process.env.REACT_APP_API_URL + 'api/station-queue?stationId=' + stationId + '&service=battery&startDate=' + startDate + '&endDate=' + endDate + '&access_token=' + token);
    return response;
}

export async function getSpawStationQueueAllDetail(stationId) {
    const token = localStorage.getItem('Access_');
    const response = await axios.get(process.env.REACT_APP_API_URL + 'api/station-queue?stationId=' + stationId + '&service=battery&access_token=' + token);
    return response;
}

export async function swapBatteryConfirm(stationQueueId) {
    const token = localStorage.getItem('Access_');
    const body = {};
    const response = await axios.post(process.env.REACT_APP_API_URL + 'api/swap-battery/confirm/' + stationQueueId + '?access_token=' + token, body);
    return response;
}

export async function swapBatteryReject(stationQueueId) {
    const token = localStorage.getItem('Access_');
    const body = {};
    const response = await axios.post(process.env.REACT_APP_API_URL + 'api/swap-battery/reject/' + stationQueueId + '?access_token=' + token, body);
    return response;
}



