import React from 'react';
import { v4 as uuidv4 } from 'uuid';
import "moment/locale/th";
import loading_pic from "../../img/loading.gif"
import BottomSheetx from "../../Pages/Bottom_Sheet/bottom_sheet.js";
class SwapBatteryListService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userGuest: null,
            nameStation: [],
            queueService: [],
            queueList: [],
            selectTime: 0,
            doing: '',
        };
    }
    async componentDidUpdate(prevProps) {
        if (prevProps.selectTime !== this.props.selectTime) {
            this.setState({
                selectTime: this.props.selectTime,
                queueList: this.props.queueList,
            });
        }
    }
    loadingScene() {
        return (
            <div className="row">
                <div className="col-12 text-center">
                    <img className="loadingPic" src={loading_pic} alt="loading_pic" />
                </div>
            </div>
        );
    }
    selectService(status, stationQueueId) {
        var doWhat;
        switch (status) {
            case 'pending':
                doWhat = 'pending'
                break;
            case 'onservice':
                doWhat = 'onservice'
                break;
            case 'expired':
                doWhat = 'expired'
                break;
            case 'finish':
                doWhat = 'finish'
                break;
            case 'cancel':
                doWhat = 'cancel'
                break;
            case 'reject':
                doWhat = 'reject'
                break;
            case 'success':
                doWhat = 'success'
                break;
            case 'confirm':
                doWhat = 'confirm'
                break;
            default:
        }
        this.setState({ stationQueueIdSelect: stationQueueId, doing: doWhat })
        setTimeout(() => {
            document.getElementById("submitBtnSheet").click();
        }, 200);
    }
    render() {
        let listItems = [];
        for (let i = 0; i < this.state.queueList.length; i++) {
            if (this.state.queueList.length > 0) {
                var status;
                if (this.state.queueList[i].status === 'onservice') {
                    status = (<button className='btn-onservice' >
                        กำลังให้บริการ
                    </button>);
                } else if (this.state.queueList[i].status === 'expired') {
                    status = (<button className='btn-expired'>
                        หมดอายุ
                    </button>);
                } else if (this.state.queueList[i].status === 'success') {
                    status = (<button className='btn-finish'>
                        บริการสำเร็จ
                    </button>);
                } else if (this.state.queueList[i].status === 'confirm') {
                    status = (<button className='btn-confirm'>
                        พร้อมบริการ
                    </button>);
                } else if (this.state.queueList[i].status === 'pending') {
                    status = (<button className='btn-pending'>
                        รอยืนยัน
                    </button>);
                } else if (this.state.queueList[i].status === 'reject') {
                    status = (<button className='btn-expired'>
                        ยกเลิก
                    </button>);
                }
                listItems.push(
                    <tr key={uuidv4()} onClick={() => this.selectService(this.state.queueList[i].status, this.state.queueList[i].stationQueueId)} style={{ cursor: "pointer" }}>
                        <td>
                            {(this.state.queueList[i].vehicleNo)}
                        </td>
                        <td>{'ไม่มีข้อมูล'}</td>
                        <td>
                            {status}
                        </td>
                    </tr>)
            }
        }
        return (
            <>
                <table className="table table-borderless text-center">
                    <thead>
                        <tr>
                            <th scope="col">ทะเบียนรถ</th>
                            <th scope="col">แบตเตอรี่</th>
                            <th scope="col">สถานะการจอง</th>
                        </tr>
                    </thead>
                    <tbody >
                        {listItems}
                    </tbody>
                </table>
                <BottomSheetx stationQueueId={this.state.stationQueueIdSelect} setWhat={this.state.doing} />
            </>
        );
    }
}
export { SwapBatteryListService }; 