import React from "react";
import QrReader from "react-qr-reader";
import { enableRipple } from "@syncfusion/ej2-base";
import QrcodeDecoder from "qrcode-decoder";
import "./styles.css";
import { BottomSheet } from 'react-spring-bottom-sheet'
import 'react-spring-bottom-sheet/dist/style.css'
import loading_pic from "../img/loading.gif"
import close from "../img/x1/close.png"
import error_icon from "../img/x2/error_icon.png"
import "../Pages/Bottom_Sheet/bottom_sheet.css";
import { stationQueueDetail } from "../services/station/station.js";
import { maintenanceOnservice, maintenanceFinish } from "../services/maintenanceService/maintenanceAPI.js"
import { spawBatteryOnservice, spawBatteryFinish } from "../services/swapBatteryService/swapBatteryAPI.js"
import arrow_left_white from "../img/x1.5/arrow_left_white.png"
import cir_plus from "../img/x1.5/cir_plus.png"
import hashtag from "../img/x1/hashtag.png"
import Add_Image from "../img/x1/Add_Image.png"
import swapCamera from "../img/x1/swapCamera_small.png"
import frameQr from "../img/frameQr.png"
import { isMobile } from "react-device-detect";
import moment from 'moment';
import "moment/locale/th";
moment.locale('th');
enableRipple(true);
class QRCode extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      delay: 300,
      result: 'No result',
      domain: "http://localhost:3000/",
      open: false,
      step: 1,
      stationQueueId: this.props.value,
      from: localStorage.getItem('comeFrom'),
      stationData: {},
      queuetime1: '',
      queuetime1_unix: '',
      queuetime2: '',
      comp: this.loadingScene(),
      stopScan: 1,
      camera: "environment",
      styleFrame: {},
      qrFrame: {},
      onWork: {},
      status: 'QR โค้ดไม่สามารถใช้ได้',
      timeText: '',
    };
    this.items = [
      {
        text: "กรอกเลข",
        url: this.state.domain + "qr",
      },
      {
        text: "สแกนจากรูป",
        url: this.state.domain,
      },
    ];
    this.autoScan = this.autoScan.bind(this);
    this.setOpen = this.setOpen.bind(this);
    this.setOnservice = this.setOnservice.bind(this);
    this.qr_reader = this.qr_reader.bind(this);
    this.swapperCamera = this.swapperCamera.bind(this);
    this.getWindowDimensions = this.getWindowDimensions.bind(this);
  }
  componentDidMount() {
    this.setState({
      camera: this.detectDevice()
    });
    this.getWindowDimensions()
  }
  swapperCamera() {
    if (this.state.camera === "environment") {
      this.setState({
        camera: "user"
      })
    } else {
      this.setState({
        camera: "environment"
      })
    }
  }
  detectDevice() {
    if (isMobile ? "Mobile" : "Desktop") {
      return "environment"
    } else {
      return "user"
    }
  }
  async autoScan(data) {
    if (data) {
      this.setState({
        result: data,
      });
      if (this.state.stopScan === 1) {
        this.setState({
          step: 1,
          comp: this.loadingScene(),
          stopScan: 2,
          open: true
        });
        setTimeout(async () => {
          await this.setOpen(true);
        }, 2000);
      }
    }
  }
  handleError(err) {
    console.error(err);
  }
  qr_reader() {
    let file = document.getElementById("file-input").files[0];
    if (file) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
        var qr = new QrcodeDecoder();
        qr.decodeFromImage(reader.result).then((res) => {
          this.setState({ result: res.data });
          this.setOpen(true)
        });
      }.bind(this);
      reader.onerror = function (error) {
      };
    }
  }
  back() {
    window.history.back();
  }
  async setOpen(val) {
    if (val) {
      if (this.state.result) {
        let result = this.state.result;
        await stationQueueDetail(result).then(
          response => {
            response = response.data;
            this.setState({
              onWork: response.service.slug
            })
           
            if (response) {
            
              let unixTimeSub10min = moment(response.queueTime).subtract(10, 'minutes').unix();
              let unixTimePlus10min = moment(response.queueTime).add(20, 'minutes').unix();
              let currentUnixTime = moment().unix();
              let stationData = JSON.parse(sessionStorage.getItem('station'));
              if (currentUnixTime < unixTimeSub10min) {
                this.setState({
                  status: 'ยังไม่ถึงช่วงเวลาที่สแกนได้',
                  timeText:'เวลาที่จอง:' + (response.queueTime).substring(11, 16) + ' น.'
                })
                this.setState({ step: 3, open: val, comp: this.errorUI() });
              }else if(currentUnixTime > unixTimePlus10min){
                this.setState({
                  status: 'เลยช่วงเวลาที่สแกนได้',
                  timeText:'เวลาที่จอง:' + (response.queueTime).substring(11, 16) + ' น.'
                })
                this.setState({ step: 3, open: val, comp: this.errorUI() });
              }
              else if (response.station.stationId === stationData.stationId) {
                if (this.state.onWork === 'battery') {
                  this.setState({
                    queuetime1: moment(response.queueTime).format('H:mm'),
                    queuetime2: moment(response.queueTime).add(30, 'Minutes').format('H:mm'),
                    queuetime1_unix: moment(response.queueTime).unix()
                  });
                } else {
                  this.setState({
                    queuetime1: moment(response.queueTime).format('H:mm'),
                    queuetime2: moment(response.queueTime).add(1, 'hour').format('H:mm'),
                    queuetime1_unix: moment(response.queueTime).unix()
                  });
                }
                this.setState({
                  stationData: response,
                });
                setTimeout(() => {
                  this.setState({ step: 2, open: val, comp: this.queueSuccess() });
                }, 500);
              } else {
                this.setState({ step: 3, open: val, comp: this.errorUI() });
              }
            }
            // }
          }).catch(
            err => {
              this.setState({
                step: 3,
                comp: this.errorUI(),
                open: val
              })
            }
          );
      }
    } else {
      this.setState({ open: val, stopScan: 1 });
    }
  }
  async setOnservice() {
    sessionStorage.setItem('prevTime', this.state.queuetime1_unix);
    if (this.state.onWork === 'battery') {
      await spawBatteryOnservice(this.state.stationData.stationQueueId).then(
        response => {
          this.setState({ step: 1 })
          if (response.data) {
            window.location.href = "/home_battery";
          }
        }
      ).catch(
        err => {
          this.setState({ step: 3 })
        }
      );
    } else {
      await maintenanceOnservice(this.state.stationData.stationQueueId).then(
        response => {
          this.setState({ step: 1 })
          if (response.data) {
            window.location.href = "/home_service";
          }
        }
      ).catch(
        err => {
          this.setState({ step: 3 })
        }
      );
    }
  }
  async setFinishservice() {
    sessionStorage.setItem('prevTime', this.state.queuetime1_unix);
    if (this.state.from === 'battery') {
      await spawBatteryFinish(this.state.stationData.stationQueueId).then(
        response => {
          this.setState({ step: 1 })
          if (response.data) {
            window.location.href = "/home_battery";
          }
        }
      ).catch(
        err => {
          this.setState({ step: 3 })
          return err;
        }
      );
    } else {
      await maintenanceFinish(this.state.stationData.stationQueueId).then(
        response => {
          this.setState({ step: 1 })
          if (response.data) {
            window.location.href = "/home_service";
          }
        }
      ).catch(
        err => {
          this.setState({ step: 3 })
          return err;
        }
      );
    }
  }
  loadingScene() {
    return (
      <div className="row">
        <div className="col-12 text-center">
          <img className="loadingPic" src={loading_pic} alt="loading_pic" />
        </div>
      </div>
    );
  }
  queueSuccess() {
    var btn_;
    var statusRender;
    if (this.state.stationData.status === 'pending') {
      statusRender = (<button className='btn-pending' >
        รอยืนยัน
      </button>)
      btn_ = (<button className="btn-main w-100" onClick={() => this.setOnservice()} >
        เริ่มบริการ
      </button>)
    } else if (this.state.stationData.status === 'confirm') {
      statusRender = (<button className='btn-confirm' >
        พร้อมบริการ
      </button>)
      btn_ = (<button className="btn-main w-100" onClick={() => this.setOnservice()} >
        เริ่มบริการ
      </button>)
    } else if (this.state.stationData.status === 'onservice') {
      statusRender = (<button className='btn-onservice' >
        กำลังให้บริการ
      </button>)
      btn_ = (<button className="btn-main w-100" onClick={() => this.setFinishservice()} >
        เสร็จสิ้น
      </button>)
    } else if (this.state.stationData.status === 'reject') {
      statusRender = (<button className='btn-expired' >
        ยกเลิก
      </button>)
      btn_ = (<button className="btn-main w-100" style={{ display: 'none' }}>
        ยกเลิก
      </button>)
    } else if (this.state.stationData.status === 'success') {
      statusRender = (<button className='btn-finish' >
        บริการสำเร็จ
      </button>)
      btn_ = (<button className="btn-main w-100" style={{ display: 'none' }}>
        บริการสำเร็จ
      </button>)
    } else if (this.state.stationData.status === 'expired') {
      statusRender = (<button className='btn-expired' >
        หมดอายุ
      </button>)
      btn_ = (<button className="btn-main w-100" style={{ display: 'none' }}>
        หมดอายุ
      </button>)
    }
    return (
      <div className="row d-flex justify-content-center align-items-center" style={{ minHeight: '255px' }}>
        <div className="col-lg-3 col-md-4 col-sm-5 col-6">
          <span className="sub-title col-2">สถานี: </span><span className="detail">{this.state.stationData.station.name}</span><br />
          <span className="sub-title col-2">จองไว้เวลา: </span><span className="detail">{this.state.queuetime1} - {this.state.queuetime2}</span><br />
          {
            this.state.onWork === 'battery' ?
              <>  {
                this.state.stationData.contract == null ?
                  <><span className="sub-title col-2">แพ็กเกจ: </span><span className="detail">ไม่มีแพ็กเกจ</span><br /></>
                  :
                  <><span className="sub-title col-2">แพ็กเกจ: </span><span className="detail">{this.state.stationData.contract.rentalPackageView}</span><br /></>
              }</>
              :
              <><span className="sub-title">หมายเหตุ: </span><span className="detail">{this.state.stationData.note}</span><br /></>
          }
          <span className="sub-title">คิวที่จอง: </span><span className="detail">{this.state.stationData.queueCode}</span><br />
        </div>
        <div className="col-lg-3 col-md-4 col-sm-5 col-6 d-flex justify-content-center align-items-center">
          <div className="row m-0 p-0 w-100">
            <div className="col-12  m-0 p-0">
              <div className="row mb-2">
                <div className="col-12">
                  <span className="sub-title-pending col-2">สถานะการบริการ </span>
                  <br />
                  {statusRender}
                </div>
              </div>
              <div className="row mb-2">
                <div className="col-12 ">
                  {btn_}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
  errorUI() {
    return (
      <>
        <div className="row">
          <div className="col-12" style={{ textAlign: "end" }}>
            <img src={close} onClick={() => this.setOpen(false)} alt="close" />
          </div>
        </div>
        <div className="row d-flex justify-content-center mb-2">
          <div className="col-12 text-center">
            <img className="errorPic" src={error_icon} alt="error_icon" />
          </div>
          <div className="col-12 text-center" style={{ marginTop: "5px" }}>
            <span className="title-err">
              {
                this.state.status
              }
              <br/>
              {
                this.state.timeText
              }
            </span>
            <br />
            <span className="sub-title">กรุณาลองใหม่อีกคร้ง</span>
          </div>
          <div className="col-lg-3 col-md-4 col-sm-5 col-6 mt-2 text-center">
            <button className="btn-main w-100" onClick={() => this.setOpen(false)}>
              ถ่ายอีกครั้ง
            </button>
          </div>
        </div>
      </>
    );
  }
  async getWindowDimensions() {
    const { innerWidth: width, innerHeight: height } = window;
    if (width === 2000 && height === 1200) {
      this.setState({
        styleFrame: { width: '100%', position: 'fixed', top: '-400px' }
      })
      this.setState({
        qrFrame: {
          marginTop: "15%",
          right: "42rem",
          position: "fixed",
          zIndex: "99",
          backgroundColor: "transparent",
          cursor: "pointer"
        }
      })
    } else if (width === 1200 && height === 2000) {
      this.setState({
        styleFrame: { width: "100vh", position: "fixed", right: "10pt", left: "-530px" }
      })
      this.setState({
        qrFrame: {
          marginTop: "50%",
          right: "19rem",
          position: "fixed",
          zIndex: "99",
          backgroundColor: "transparent",
          cursor: "pointer"
        }
      })
    } else {
      this.setState({
        styleFrame: { width: "100%", height: "100%" }
      })
      this.setState({
        qrFrame: {
          width: "70%",
          height: "40%",
          top: "15%",
          left: "15%",
          position: "fixed",
          zIndex: "99",
          backgroundColor: "transparent",
          cursor: "pointer"
        }
      })
    }
  }
  render() {
    return (
      <div>
        <div className="dropleft" >
          <img src={cir_plus} alt="cir_plus" style={styles.top} type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" />
          <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton1" style={{ inlineSize: "fit-content" }}>
            <li >
              <a className="dropdown-item " href="/input_number" >
                กรอกเลข
                <img src={hashtag} className="icon-dropDown" alt="hashtag" />
              </a>
            </li>
            <li >
              <button className="dropdown-item" >
                <input id="file-input" type="file" accept="image/*" style={{ display: "none" }} onChange={this.qr_reader} />
                <label htmlFor="file-input" className="dropdown-item forLabel">
                  สแกนจากรูป
                  <img src={Add_Image} className="icon-dropDown" alt="Add_Image" />
                </label>
              </button>
            </li>
            <li >
              <button className="dropdown-item" onClick={this.swapperCamera} >
                สลับกล้อง
                <img src={swapCamera} className="icon-dropDown" alt="swapCamera" />
              </button>
            </li>
          </ul>
        </div>
        <img src={arrow_left_white} style={styles.icon} onClick={this.back} alt="arrow_left_white" />
        <img src={frameQr} style={this.state.qrFrame} alt="frameQr" />
        <QrReader
          delay={this.state.delay}
          style={this.state.styleFrame}
          onError={this.handleError}
          onScan={this.autoScan}
          facingMode={this.state.camera}
          showViewFinder={false}
        />
        <BottomSheet open={this.state.open} onDismiss={() => this.setOpen(false)} blocking={false} snapPoints={({ maxHeight }) => [276, 276]} style={{ backgroundColor: "white" }}>
          <div className="container">
            {this.state.comp}
          </div>
        </BottomSheet>
      </div>
    );
  }
}
var styles = {
  icon: {
    marginTop: "3%",
    left: "2%",
    fontSize: "2em",
    position: "fixed",
    zIndex: "99",
    backgroundColor: "transparent",
    cursor: "pointer"
  },
  top: {
    marginTop: "3%",
    right: "2%",
    position: "fixed",
    zIndex: "99",
    backgroundColor: "transparent",
    cursor: "pointer"
  },
  center_top: {
  }
};
export default QRCode;
