import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import reportWebVitals from "./reportWebVitals";
import { Alert, AlertTitle } from '@mui/material';
import { Offline, Online } from "react-detect-offline";

setInterval
  (
    () => {
      document.getElementById('speed-check').click();
      setTimeout
        (
          () => {
            document.getElementById('result').innerHTML = "";
          },
          80000
        )
    },
    100000
  )

ReactDOM.render(

  <React.StrictMode>

    <link href="//cdn.syncfusion.com/ej2/ej2-base/styles/material.css" rel="stylesheet" />
    <link href="//cdn.syncfusion.com/ej2/ej2-buttons/styles/material.css" rel="stylesheet" />
    <link href="//cdn.syncfusion.com/ej2/ej2-popups/styles/material.css" rel="stylesheet" />
    <link href="//cdn.syncfusion.com/ej2/ej2-splitbuttons/styles/material.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/systemjs/0.19.38/system.js"></script>
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
    />
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
    />
    <Offline> <Alert severity="error" variant="filled"  >
      <AlertTitle>Error</AlertTitle>
      No Internet — <strong className="MuiTypography-root" >please check your internet connect!</strong>
    </Alert></Offline>
    <Alert severity="error" variant="filled" style={{ display: "none" }}>
      <AlertTitle>Error</AlertTitle>
      No Internet — <strong className="MuiTypography-root" >please check your internet connect!</strong>
    </Alert>
    <Alert severity="warning" variant="filled" style={{ display: "none" }}>
      <AlertTitle>Warning</AlertTitle>
      Slow Internet — <strong className="MuiTypography-root" >please check your internet!</strong>
    </Alert>
    <button className="btn btn btn-success btn-lg" id="speed-check" style={{ display: "none" }}>Start</button>
    <div id="result"></div>
    {/* <button className="add-button" >ADD TO HOME SCREEN</button> */}
    <App />
  </React.StrictMode>,
  document.getElementById("root"),
  
);
// let deferredPrompt;

// const addBtn = document.querySelector('.add-button');
// addBtn.style.display = 'none';
// window.addEventListener('beforeinstallprompt', (e) => {
//   // Prevent Chrome 67 and earlier from automatically showing the prompt
//   e.preventDefault();
//   // Stash the event so it can be triggered later.
//   deferredPrompt = e;
//   // Update UI to notify the user they can add to home screen
//   addBtn.style.display = 'block';
//   // addBtn.setAttribute("class", "add-button");
  
//   // setTimeout(() => {
//   //   addBtn.style.display = 'none';
//   // }, 3000);

//   addBtn.addEventListener('click', (e) => {
//     // hide our user interface that shows our A2HS button
//     addBtn.style.display = 'none';
//     // Show the prompt
//     deferredPrompt.prompt();
//     // Wait for the user to respond to the prompt
//     deferredPrompt.userChoice.then((choiceResult) => {
//       if (choiceResult.outcome === 'accepted') {
//         console.log('User accepted the A2HS prompt');
//       } else {
//         console.log('User dismissed the A2HS prompt');
//       }
//       deferredPrompt = null;
//     });
//   });
// });
//  setTimeout(() => {
//         const addBtn = document.querySelector('.add-button');
//         addBtn.style.display = 'none';
//       }, 2000)


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
var speedButton = document.getElementById('speed-check');
speedButton.addEventListener('click', InitiateSpeedDetection, false);
var imageAddr = "https://hackthestuff.com/images/test.jpg";
// var imageAddr = "https://doc-0g-9c-docs.googleusercontent.com/docs/securesc/a3c574raepjo5i9d2k5hoi6rqkl6bcud/q6rdhv1i5lbadnel2nla3k49eupbemt9/1645412025000/00371165173078917421/00371165173078917421/17znlXqHPQOSNu_f5OUP5JgoQadrxqY_K?e=download&ax=ACxEAsYUIaKbUvVN_TF8ENiWVsxHKbMK8BmtckKWrsXu02ThBdj5TEGgjVfAprNPWGbn1PIgEmqV7fp3vHgK6_gWex3llNZvlJ6jZt9AcYaCqm8OMlFAgjGThF4PTAAOA43WIywzg3WZG5L7ziTkcO6KJ2i9V_ip02RCHvt7LA59HI06yjZHtKJ2WRCMzTWoEmEVIhExVQoZvjhnPsSZXk4nu3icoeLAIONCxsMA9vAbmn6RzG6q4dB8FU9VaN8lXtRetkDMlKRxqJiMuzg-6iRp_5bKAo-dT2TuxepnNqlBHNcuzalYRHkRxhfN8-IiKpQFv8VGOgRXXkFHRnNadsuFo_euRQXOPudfUxVShMtseuM1J-TXd7UJcY0YR4lFdQCn0ZV2O_oWv_lOWK-FWNWm0JQcS6IeuAmNi2M9JRd9q7ZR-m0h-8i_vxubmMp8-yN1pVnhz7Q56Qg_vTG4wnF1FqsBRY7ytInBW7VB9vVRKYikjFwPj8rki7drwK1dbBhgsVbGUg3oCd0MA3VyUK0xuU0zP9KgdeAC546Jfc0NPzGmFNNAEyspTK2j3dNr40ZdtivQJ2tkF0jWmVd5rAgfhyZ1bzB6yEXf9J9ISIoi12GGLcgcDhAmnEczD1JIf437t8j8zPF59j62cM97Vddk3VAk57x95nSDpqArvVI&authuser=0&nonce=mafh51ic7flsc&user=00371165173078917421&hash=i161i6a2ootba2k1n11l8fr50e7ig62v";
var downloadSize = 13055440;
async function ShowProgressMessage(msg, speed, err) {
  var progress = await document.getElementById("result");
  if (speed < 2) {
    progress.innerHTML = ` <div class="MuiPaper-root MuiPaper-elevation MuiPaper-rounded MuiPaper-elevation0 MuiAlert-root MuiAlert-filledWarning MuiAlert-filled css-bevsf8-MuiPaper-root-MuiAlert-root" role="alert"><div class="MuiAlert-icon css-1ytlwq5-MuiAlert-icon"><svg class="MuiSvgIcon-root MuiSvgIcon-fontSizeInherit css-1vooibu-MuiSvgIcon-root" focusable="false" aria-hidden="true" viewBox="0 0 24 24" data-testid="ReportProblemOutlinedIcon"><path d="M12 5.99L19.53 19H4.47L12 5.99M12 2L1 21h22L12 2zm1 14h-2v2h2v-2zm0-6h-2v4h2v-4z"></path></svg></div><div class="MuiAlert-message css-acap47-MuiAlert-message"><div class="MuiTypography-root MuiTypography-body1 MuiTypography-gutterBottom MuiAlertTitle-root css-yslrua-MuiTypography-root-MuiAlertTitle-root">Warning</div>Slow Internet — <strong class="MuiTypography-root">please check your internet!</strong></div></div>
    </div>`;
  }
}
function InitiateSpeedDetection() {
  ShowProgressMessage("Checking speed, please wait...");
  window.setTimeout(MeasureConnectionSpeed, 0);
};
function MeasureConnectionSpeed() {
  var startTime, endTime;
  var download = new Image();
  download.onload = function () {
    endTime = (new Date()).getTime();
    showResults();
  }
  download.onerror = function (err, msg) {
    ShowProgressMessage("Invalid image, or error downloading", msg, err);
  }
  startTime = (new Date()).getTime();
  var cacheBuster = "?nnn=" + startTime;

  download.src = imageAddr + cacheBuster;
  function showResults() {
    var duration = (endTime - startTime) / 1000;
    var bitsLoaded = downloadSize * 8;
    var speedBps = (bitsLoaded / duration).toFixed(2);
    var speedKbps = (speedBps / 1024).toFixed(2);
    var speedMbps = (speedKbps / 1024).toFixed(2);
    if (speedMbps > 1) {
      ShowProgressMessage("Your connection speed is " + speedMbps + " Mbps", speedMbps);
    } else if (speedKbps > 1) {
      ShowProgressMessage("Your connection speed is " + speedKbps + " kbps", speedKbps);
    } else {
      ShowProgressMessage("Your connection speed is " + speedBps + " bps", speedBps);
    }
  }
}
