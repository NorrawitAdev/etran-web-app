import QRCode from "../Pages/QRCode.js";
import Topbar from "../Pages/Topbar/topbar.js";
import Welcome from "../Pages/Welcome/welcome.js";
import ChooseMenu from "../Pages/Choose_Menu/choose_menu.js";
import InputNumber from "../Pages/Input_Number/inputNumber.js";
import Error from "../Pages/error.js";
import React, { Component } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
class RouteX extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
    };
  }
  render() {
    return (
      <Router>
        <Routes>
          <Route exact path="/" element={<Welcome />} />
          <Route exact path="/menu" element={<ChooseMenu />} />
          <Route exact path="/qr" element={<QRCode />} />
          <Route exact path="/error" element={<Error />} />
          <Route
            exact
            path="/home_battery"
            element={<Topbar toggle={this.state.toggle} params="battery" />}
          />
          <Route
            exact
            path="/home_service"
            element={<Topbar toggle={this.state.toggle} params="service" />}
          />
          <Route exact path="/input_number" element={<InputNumber />} />
    
        </Routes>
      </Router>
    );
  }
} 
export default RouteX;
